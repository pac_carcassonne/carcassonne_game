#pragma once

#include "Tile.h"

#include <random>
#include <algorithm>
#include <array>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <iostream>
#include <ctime>

class UnusedTiles
{
public:
	UnusedTiles();

	~UnusedTiles() = default;

public:
	void EmplaceTile(Tile&& tile);

	void GenerateTiles();

	void Shuffle();

	void Rotation();

	Tile& GetTile();

private:
	Feature m_up;
	Feature m_right;
	Feature m_middle;
	Feature m_bottom;
	Feature m_left;
	std::vector<Tile> m_deck;
	int nr;
};

