#pragma once
#include "Feature.h"
#include "Follower.h"

class Side
{
public:
	Side(Feature feature);

	Feature GetFeature();

	void AddFollower(Follower follower);

	void RemoveFollower(Follower follower);

private:
	Feature m_leftFeature;
	Feature m_middleFeature;
	Feature m_rightFeature;
	Follower m_leftFollower;
	Follower m_follower;
	Follower m_rightFollower;

};