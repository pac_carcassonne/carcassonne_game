#pragma once
#include "Tile.h"
#include<vector>
#include<utility>

class Board
{
public:
	Board(Tile &startTile);

	~Board() = default;

	void PushDownLines();

	void PushRightColumns();

	void AddTile(const int& x, const int& y, Tile* tile);

	bool IsFreePosition(int x, int y);

	bool IsValidPosition(const int& x, const int& y, Tile* tile);

	void possiblePositions(Tile* tile);

private:
	int m_maxRight;
	int m_maxBottom;
	//static int m_numberOfTiles;
	std::vector<std::vector<Tile*>> m_board;
};