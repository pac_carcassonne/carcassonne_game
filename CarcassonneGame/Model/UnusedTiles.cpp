#include "UnusedTiles.h"

UnusedTiles::UnusedTiles() {
	std::srand(std::time(nullptr));

	nr = -1;

	GenerateTiles();
}

Feature FromStringToFeature(const std::string& sideName)
{
	Feature feature;

	if (sideName == "Road")
	{
		feature = Feature::Road;
	}
	else
		if (sideName == "Cloister")
		{
			feature = Feature::Cloister;
		}
		else
			if (sideName == "City")
			{
				feature = Feature::City;
			}
			else
				if (sideName == "Field")
				{
					feature = Feature::Field;
				}
				else
					if (sideName == "RoadCross")
					{
						feature = Feature::RoadCross;
					}

	return feature;
}

void UnusedTiles::EmplaceTile(Tile && tile)
{
	m_deck.push_back(tile);
}

void UnusedTiles::GenerateTiles()
{
	std::ifstream inputFile("tilesFeatures.txt");

	std::string up, right, middle, bottom, left, name;
	int numberOfTiles;

	while (!inputFile.eof())
	{
		inputFile >> up >> right >> middle >> bottom >> left;
		inputFile >> numberOfTiles;
		inputFile >> name;

		Feature upSide = FromStringToFeature(up);
		Feature rightSide = FromStringToFeature(right);
		Feature middleSide = FromStringToFeature(middle);
		Feature bottomSide = FromStringToFeature(bottom);
		Feature leftSide = FromStringToFeature(left);

		for (int index = 0; index < numberOfTiles; ++index)
		{
			Tile currentTile(upSide, rightSide, middleSide, bottomSide, leftSide, name);
			EmplaceTile(std::move(currentTile));
		}
	}
}

void Swap(Tile& firstTile, Tile& secondTile)
{
	Tile auxiliaryTile(firstTile);
	firstTile = secondTile;
	secondTile = auxiliaryTile;
}

void UnusedTiles::Shuffle()
{
	std::random_device randomDevice;
	std::mt19937 generator(randomDevice());
	std::uniform_int_distribution<> distribution(0, 71);
	for (int index = 0; index < m_deck.size(); ++index)
	{
		int swapIndex = distribution(generator);
		Swap(m_deck[index], m_deck[swapIndex]);
	}
}

void UnusedTiles::Rotation()
{
	Feature aux = m_up;
	m_up = m_right;
	m_right = m_bottom;
	m_bottom = m_left;
	m_left = aux;
}

Tile& UnusedTiles::GetTile()
{
	nr++;
	return m_deck[nr];
}


