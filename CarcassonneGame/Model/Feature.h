#pragma once

enum class Feature 
{
	Road,
	Cloister,
	City,
	Field,
	RoadCross,
	Empty
};