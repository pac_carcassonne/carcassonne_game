#pragma once

#include "Player.h"

#include <vector>
#include <utility>

class ScoreTable
{
public:
	void UpdatePlayerScore(const Player& currentPlayer, const int& numberOfPoints);

	int GetNumberOfPlayers();

	void AddPlayer(const Player& newPlayer);

private:
	std::vector<std::pair<Player, int>> m_playersScore;
};

