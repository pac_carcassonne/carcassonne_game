#include "Side.h"

Side::Side(Feature feature)
{
	switch (feature)
	{
	case Feature::Road:
		m_leftFeature = Feature::Field;
		m_middleFeature = Feature::Road;
		m_rightFeature = Feature::Field;
		break;
	case Feature::Field:
		m_leftFeature= Feature::Field;
		m_middleFeature= Feature::Field; 
		m_rightFeature = Feature::Field;
		break;
	case Feature::City:
		m_leftFeature = Feature::City;
		m_middleFeature = Feature::City;
		m_rightFeature = Feature::City;
		break;
	case Feature::RoadCross:
		m_middleFeature = Feature::RoadCross;
		break;
	case Feature::Cloister:
		m_middleFeature = Feature::Cloister;
		break;
	case Feature::Empty:
		m_middleFeature = Feature::Empty;
		break;
	}

}

Feature Side::GetFeature()
{
	return m_middleFeature;
}

void Side::AddFollower(Follower follower)
{
	this->m_follower = follower;
}

void Side::RemoveFollower(Follower follower)
{
	follower.Remove();
}
