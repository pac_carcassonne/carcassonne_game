#include "MenuGUI.h"

MenuGUI::MenuGUI()
{
	m_backgroundTexture = new ImageObject("assets/menu_background.jpeg");
	logger.Log("Menu backround texture created", Logger::Level::Info);

	m_localGameButton = new ImageObject( "assets/local_game.jpeg", 64, 250, 250, 515);
	logger.Log("new game button texture created", Logger::Level::Info);

	m_onlineGameButton = new ImageObject("assets/online_game.jpeg", 64, 250, 780, 515);
	logger.Log("new game button texture created", Logger::Level::Info);
}

ApplicationStatus MenuGUI::Run()
{
	while (IsRunning())
	{
		SDL_RenderClear(RendererManager::m_renderer);

		m_backgroundTexture->Render();
		m_localGameButton->Render();
		m_onlineGameButton->Render();

		SDL_RenderPresent(RendererManager::m_renderer);

		std::optional<std::tuple<int, int>> coordinates = MouseClick();
		if (coordinates)
		{
			int x, y;
			std::tie(x, y) = *coordinates;

			if (m_localGameButton->Clicked(x, y))
			{
				logger.Log("local game button was clicked", Logger::Level::Info);
				return ApplicationStatus::InGame;
			}

			if (m_onlineGameButton->Clicked(x, y))
			{
				logger.Log("online game button was clicked", Logger::Level::Info);
				return ApplicationStatus::InOnlineMenu;
			}
		}
	}

	return ApplicationStatus::Exit;
}
