#pragma once

#include "ImageObject.h"

#include <vector>

class BoardView : public ImageObject
{
public:
	BoardView(const char* imageFileName,
		const int& height = 640, const int& width = 820, const int& xpos = 420, const int& ypos = 40);

	void Render() override;

	std::pair<int,int> PlaceTile(int x, int y, char* name);

private:
	std::vector < std::vector<ImageObject*> > m_board;
};

