#include "TextObject.h"

TextObject::TextObject(const char* text, const int & height, const int & width, const int & xpos, const int & ypos)
{
	TTF_Font* font = TTF_OpenFont("../ThirdParty/Champagne.ttf", 24);
	SDL_Color color = { 255, 255, 255 };

	SDL_Surface* surface = TTF_RenderText_Solid(font, text, color);
	m_texture = SDL_CreateTextureFromSurface(RendererManager::m_renderer, surface);
	SDL_FreeSurface(surface);

	m_rectangle.h = height;
	m_rectangle.w = width;
	m_rectangle.x = xpos;
	m_rectangle.y = ypos;
}
