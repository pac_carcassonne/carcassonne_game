#include "ApplicationWindow.h"

ApplicationWindow::ApplicationWindow(const char* title, int height, int width, int xpos, int ypos, Uint32 flags) 
{
	SDL_Init(SDL_INIT_EVERYTHING);

	m_window = SDL_CreateWindow(title, xpos, ypos, width, height, flags);
	logger.Log("Window initialized", Logger::Level::Info);

	RendererManager::InitializeRenderer(m_window);
}


void ApplicationWindow::Run()
{
	bool isRunning = true;
	ApplicationStatus m_status = ApplicationStatus::InMenu;

	while (isRunning)
	{
		switch (m_status)
		{
		case ApplicationStatus::InMenu:
		{
			MenuGUI menu;
			m_status = menu.Run();
			break;
		}
		case ApplicationStatus::InPlayersNameMenu:
		{
			PlayersNameMenuGUI playersNameMenu;
			m_status = playersNameMenu.Run();
			break;
		}
		case ApplicationStatus::InGame:
		{
			GameGUI game;
			m_status = game.Run();
			break;
		}
		case ApplicationStatus::InResults:
		{
			ResultsGUI results;
			m_status = results.Run();
			break;
		}
		case ApplicationStatus::Exit:
		{
			isRunning = false;
		}
		}
	}

	Close();
}


void ApplicationWindow::Close()
{
	SDL_DestroyWindow(m_window);
	logger.Log("Window destroyed", Logger::Level::Info);

	RendererManager::DestroyRenderer();

	SDL_Quit();
}
