#pragma once

#include"SDL/SDL.h"
#include"../../LoggingLibrary/Logging.h"

extern Logger logger;

class RendererManager
{
public:
	static void InitializeRenderer(SDL_Window* window);

	static void DestroyRenderer();

public:
	static SDL_Renderer *m_renderer;
};

