#include "ResultsGUI.h"

ResultsGUI::ResultsGUI()
{
	m_backgroundTexture = new ImageObject("assets/Wooden_Background.jpg");
	logger.Log("Menu backround texture created", Logger::Level::Info);

	m_continueButton = new ImageObject("assets/continue.png", 64, 250, 500, 515);
	logger.Log("continue button texture created", Logger::Level::Info);
}

ApplicationStatus ResultsGUI::Run()
{
	while (IsRunning())
	{
		SDL_RenderClear(RendererManager::m_renderer);

		m_backgroundTexture->Render();
		m_continueButton->Render();

		SDL_RenderPresent(RendererManager::m_renderer);
	}

	return ApplicationStatus::Exit;
}
