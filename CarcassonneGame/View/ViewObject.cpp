#include "ViewObject.h"

void ViewObject::Update(const int& xpos, const int& ypos)
{
	m_rectangle.x = xpos;
	m_rectangle.y = ypos;
}

void ViewObject::Render()
{
	SDL_RenderCopy(RendererManager::m_renderer, m_texture, NULL, &m_rectangle);
}

bool ViewObject::Clicked(const int& x, const int& y)
{
	if (x >= m_rectangle.x && x <= m_rectangle.x + m_rectangle.w &&
		y >= m_rectangle.y && y <= m_rectangle.y + m_rectangle.h)
	{
		return true;
	}

	return false;
}
