#pragma once

#include "WindowContent.h"

class MenuGUI : public WindowContent
{
public:
	//Initialize menu GUI
	MenuGUI();

	//run menu GUI (infinite loop)
	ApplicationStatus Run() override;

private:
	ImageObject* m_backgroundTexture;
	ImageObject* m_localGameButton;
	ImageObject* m_onlineGameButton;
};
