#include "ImageObject.h"

ImageObject::ImageObject(const char * imageFileName, const int & height, const int & width, const int & xpos, const int & ypos)
{
	SDL_Surface* image = IMG_Load(imageFileName);
	m_texture = SDL_CreateTextureFromSurface(RendererManager::m_renderer, image);
	SDL_FreeSurface(image);

	m_rectangle.h = height;
	m_rectangle.w = width;
	m_rectangle.x = xpos;
	m_rectangle.y = ypos;
}
