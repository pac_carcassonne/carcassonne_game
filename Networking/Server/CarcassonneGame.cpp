#include "CarcassonneGame.h"

CarcassonneGame::CarcassonneGame()
{
	application = new ApplicationWindow("CarcassonneGame");

	MenuGUI menu;
	
	switch (menu.Run())
	{
		case ApplicationStatus::InGame:
		{
			RunLocal();
			break;
		}
		case ApplicationStatus::InOnlineMenu:
		{
			RunOnline();
			break;
		}
		case ApplicationStatus::Exit:
		{
			application->Close();
		}
	}

}

void CarcassonneGame::RunLocal()
{
	Tile startingTile(Feature::City, Feature::Road, Feature::Road, Feature::Field, Feature::Road, "a");
	Board board(startingTile);
	Follower follower;
	int kNumberOfFollowers = 8;

	std::string playerName;
	std::string color = "";

	std::cout << "First player name: ";
	std::cin >> playerName;

	firstPlayer = new Player(playerName);

	std::cout << "First player choses a color: (Red, Green, Blue, Yellow, Black): ";

	while (true)
	{
		try
		{
			std::cin >> color;
			firstPlayer->SetColor(color);
			break;
		}
		catch (const char* errorMessage)
		{
			std::cout << errorMessage;
		}
	}

	for (int index = 0; index < kNumberOfFollowers; ++index)
	{
		firstPlayer->SetFollowers(kNumberOfFollowers);
	}

	std::cout << "Second player name: ";
	std::cin >> playerName;

	secondPlayer = new Player(playerName);

	std::cout << "Second player chooses a color: (Red, Green, Blue, Yellow, Black): ";

	while (true)
	{
		try
		{
			std::cin >> color;
			secondPlayer->SetColor(color);
			if (secondPlayer->GetColor() == firstPlayer->GetColor())
				throw "pick other color: ";
			break;
		}
		catch (const char* errorMessage)
		{
			std::cout << errorMessage;
		}
	}

	for (int index = 0; index < kNumberOfFollowers; ++index)
	{
		secondPlayer->SetFollowers(kNumberOfFollowers);
	}

	std::reference_wrapper<Player> pickingPlayer = *firstPlayer;
	std::reference_wrapper<Player> waitingPlayer = *secondPlayer;

	UnusedTiles unusedTiles;
	unusedTiles.Shuffle();

	Tile unusedTile = unusedTiles.GetTile();
	auto randomTile = std::make_shared<Tile>(unusedTile);
	std::shared_ptr<Tile> pickedTile(randomTile);

	GameGUI game;
	while (game.Update(*pickedTile) == ApplicationStatus::InGame)
	{ 
		pickedTile = randomTile;

		unusedTile = unusedTiles.GetTile();

		randomTile = std::make_shared<Tile>(unusedTile);
	}

	//std::swap(pickingPlayer, waitingPlayer);
}

void CarcassonneGame::RunOnline()
{
	OnlineMenuGUI onlineMenu;
	
	switch(onlineMenu.Run())
	{ 
		case ApplicationStatus::InCreateServer:
		{
			CreateServer();
			break;
		}
		case ApplicationStatus::InJoinServer:
		{
			JoinServer();
			break;
		}
		case ApplicationStatus::Exit:
		{
			application->Close();
		}
	}
	
}

int ServerManager()
{
	Server server;

	server.Initialize();
	server.CreateSocket();
	server.BindingSocket();
	server.ListeningOnSocket();
	server.AcceptConnection();
	server.SendData();
	server.Disconnect();
	server.Cleanup();

	return 0;
}

void CarcassonneGame::CreateServer()
{
	CreateServerGUI createServerGUI;

	std::thread createServer(ServerManager);

	JoinServer();

	while (createServerGUI.Run() != ApplicationStatus::Exit)
	{
		//createServer.join();
	}

	OnlineGame();
}

void CarcassonneGame::JoinServer()
{
	try
	{
		std::ifstream ipAdress("..//serverIP.txt");

		std::string ip;
		ipAdress >> ip;

		m_client.SetIP(ip);
	}
	catch (const std::exception& currentException)
	{
		currentException.what();;
	}

	m_client.InitializeWinsocket();
	m_client.CreateSocket();
	m_client.ConnectToServer();
	m_client.SendData();
	m_client.Cleanup();
}

void CarcassonneGame::OnlineGame()
{
	Tile startingTile(Feature::City, Feature::Road, Feature::Road, Feature::Field, Feature::Road, "a");
	Board board(startingTile);

	UnusedTiles unusedTiles;
	unusedTiles.Shuffle();

	Tile unusedTile = unusedTiles.GetTile();
	auto randomTile = std::make_shared<Tile>(unusedTile);
	std::shared_ptr<Tile> pickedTile(randomTile);

	GameGUI game;
	while (game.Update(*pickedTile) == ApplicationStatus::InGame)
	{
		pickedTile = randomTile;

		unusedTile = unusedTiles.GetTile();

		randomTile = std::make_shared<Tile>(unusedTile);

		if (game.WaitOpponent() == ApplicationStatus::Exit)
			break;
	}

	m_client.Cleanup();

	application->Close();
}
