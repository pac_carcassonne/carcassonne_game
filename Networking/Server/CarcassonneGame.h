#pragma once

#include "ApplicationWindow.h"
#include "Board.h"
#include "UnusedTiles.h"
#include "Player.h"
#include "Follower.h"
#include "FollowerType.h"
#include "Server.h"
#include "../Client/Client.h"

#include <memory>
#include <stdlib.h>
#include <string>
#include <thread>

class CarcassonneGame
{
public:
	CarcassonneGame();

	void RunLocal();
	void RunOnline();
	void CreateServer();
	void JoinServer();
	void OnlineGame();

private:
	ApplicationWindow* application;

	Player* firstPlayer;
	Player* secondPlayer;

	Client m_client;
};

