#include <iostream>
#include <string>

#include <winsock2.h>	
#include <ws2tcpip.h>

#pragma comment(lib, "Ws2_32.lib")	

class Server 
{
public:
	void Initialize();

	// *** Create socket ***
	void CreateSocket();
	
	// *** Binding the socket ***
	void BindingSocket();
	
	// *** Listening on the socket ***
	void ListeningOnSocket();

	// *** Accept a connection ***
	void AcceptConnection();

	// *** Recieve and send data ***
	void SendData();

	// *** Disconnecting the Server ***
	void Disconnect();

	void Cleanup();

public:
	static const uint8_t versionNumber = 2;
	static const uint8_t numberOfParameters = 2;
	static const uint8_t returnCode = 1;
	static const uint8_t mimimumLength = 0;

	static char kPortNumber[6];
	static const int kBufferLength = 512;

private:
	int m_initializeResult;

	struct addrinfo *m_result = nullptr;

	SOCKET m_listenSocket;
	SOCKET m_clientSocket;
};