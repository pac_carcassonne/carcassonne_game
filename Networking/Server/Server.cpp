#include "Server.h"

char Server::kPortNumber[6] = "27015";

void Server::Initialize()
{
	try
	{
		WSADATA wsaData;

		m_initializeResult = WSAStartup(MAKEWORD(versionNumber, versionNumber), &wsaData);		// initiate use of WS2_32.dll, with version 2.2

		if (m_initializeResult != mimimumLength)
		{
			throw "WSAStartup failed: m_initializeResult != 0";
		}
	}
	catch (const char* errorMessage)
	{
		std::cout << errorMessage << std::endl;
	}
	
}

void Server::CreateSocket()
{
	try
	{
		struct addrinfo *ptr = nullptr, hints;

		ZeroMemory(&hints, sizeof(hints));	// memset to 0
		hints.ai_family = AF_INET;			// IPv4
		hints.ai_socktype = SOCK_STREAM;	// stream
		hints.ai_protocol = IPPROTO_TCP;	// tcp
		hints.ai_flags = AI_PASSIVE;		// we intend to use the socket in a call to bind

		// Resolve the local address and port to be used by the server
		m_initializeResult = getaddrinfo(nullptr, kPortNumber, &hints, &m_result);

		if (m_initializeResult != mimimumLength)
		{
			std::cout << "getaddrinfo failed: " << m_initializeResult << std::endl;

			WSACleanup();
		}

		m_listenSocket = INVALID_SOCKET;
		// Create a SOCKET for the server to listen for client connections
		m_listenSocket = socket(m_result->ai_family, m_result->ai_socktype, m_result->ai_protocol);

		if (m_listenSocket == INVALID_SOCKET)
		{
			std::cout << "Error at socket(): " << WSAGetLastError() << std::endl;

			freeaddrinfo(m_result);

			WSACleanup();
		}
	}
	catch (const std::exception& currentException)
	{
		currentException.what();
	}
}

void Server::BindingSocket()
{
	try
	{
		// Setup the TCP listening socket
		m_initializeResult = bind(m_listenSocket, m_result->ai_addr, (int)m_result->ai_addrlen);

		if (m_initializeResult == SOCKET_ERROR)
		{
			std::cout << "Bind failed with error: " << WSAGetLastError() << std::endl;

			freeaddrinfo(m_result);

			closesocket(m_listenSocket);

			WSACleanup();
		}

		// 'result' not needed anymore so free it
		freeaddrinfo(m_result);
	}
	catch (const std::exception& currentException)
	{
		currentException.what();
	}
}

void Server::ListeningOnSocket()
{
	try
	{
		if (listen(m_listenSocket, SOMAXCONN) == SOCKET_ERROR)
		{
			std::cout << "Listen failed with error :" << WSAGetLastError() << std::endl;

			closesocket(m_listenSocket);

			WSACleanup();
		}
	}
	catch (const std::exception& currentException)
	{
		currentException.what();
	}
}

void Server::AcceptConnection()
{
	try
	{
		// There are multiple techniques to accept client connections,
		// and multiple techniques to accept multiple client connections.
		// In this example however a single connection is accepted.
		m_clientSocket = INVALID_SOCKET;

		// Accept a client socket
		m_clientSocket = accept(m_listenSocket, nullptr, nullptr);

		if (m_clientSocket == INVALID_SOCKET)
		{
			std::cout << "Accept failed:" << WSAGetLastError() << std::endl;

			closesocket(m_listenSocket);

			WSACleanup();
		}

		// No longer need server socket
		closesocket(m_listenSocket);
	}
	catch (const std::exception& currentException)
	{
		currentException.what();
	}
}

void Server::SendData()
{
	try
	{
		char receivedBuffer[kBufferLength] = "";
		char sendBuffer[kBufferLength] = "";
		int iReceivedResult, iSendResult;
		int receivedBufferLength = kBufferLength;

		// Receive until the peer shuts down the connection
		do
		{
			iReceivedResult = recv(m_clientSocket, receivedBuffer, receivedBufferLength, mimimumLength);

			if (iReceivedResult > mimimumLength)
			{
				std::cout << "Bytes received: " << iReceivedResult << std::endl;

				std::cout << "Buffer content: " << receivedBuffer << std::endl;

				// Thank the client :)
				const char* toSendBuffer = "Thank you, I received your message.";

				for (int index = 0; index < strlen(toSendBuffer); ++index)
				{
					sendBuffer[index] = toSendBuffer[index];
				}

				iSendResult = send(m_clientSocket, sendBuffer, strlen(sendBuffer), mimimumLength);

				if (iSendResult == SOCKET_ERROR)
				{
					std::cout << "Send failed: " << WSAGetLastError() << std::endl;

					closesocket(m_clientSocket);

					WSACleanup();
				}

				std::cout << "Bytes sent: " << iSendResult << std::endl;
			}
			else
			{
				if (iReceivedResult == mimimumLength)
				{
					std::cout << "Connection closing..." << std::endl;
				}
				else
				{
					std::cout << "Received failed: " << WSAGetLastError() << std::endl;

					closesocket(m_clientSocket);

					WSACleanup();
				}
			}

		} while (iReceivedResult > mimimumLength);
	}
	catch (const std::exception& currentException)
	{
		currentException.what();
	}
}

void Server::Disconnect()
{
	try
	{
		// shutdown the send half of the connection since no more data will be sent
		m_initializeResult = shutdown(m_clientSocket, SD_SEND);

		if (m_initializeResult == SOCKET_ERROR)
		{
			std::cout << "Shutdown failed: " << WSAGetLastError() << std::endl;

			closesocket(m_clientSocket);

			WSACleanup();
		}
	}
	catch (const std::exception& currentException)
	{
		currentException.what();
	}
}

void Server::Cleanup()
{
	closesocket(m_clientSocket);

	WSACleanup();
}
