#pragma once

#include "WindowContent.h"
#include "Player.h"

class GameGUI : public WindowContent
{
public:
	//Initialize game GUI
	GameGUI();

	//run game GUI (infinite loop)
	ApplicationStatus Run() override;

	//updates the content of GUI
	ApplicationStatus Update(const Tile& tile);

	ApplicationStatus WaitOpponent();

private:
	//initialize player color visual object
	void InitPlayerColor(const Player::Color& playerColor);

private:
	ViewObject* m_backgroundTexture;
	ImageObject* m_currentPlayerColor;
	BoardView* m_board;
};
