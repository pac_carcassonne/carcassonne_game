#pragma once

#include<iostream>
#include <winsock2.h>
#include <ws2tcpip.h>

#pragma comment(lib, "Ws2_32.lib")	//  indicates to the linker that the Ws2_32.lib

class Client 
{
public:
	Client();

	Client(const std::string& argv,addrinfo* ptr = nullptr, addrinfo* result = nullptr);

	Client(const Client& other);

	void SetIP(const std::string& ip);

	void InitializeWinsocket();

	void CreateSocket();

	void ConnectToServer();

	void SendData();

	void ShutdownSending();

	void ReceiveData();

	void Cleanup();

	~Client();

public:
	static const uint8_t versionNumber = 2;
	static const uint8_t numberOfParameters = 2;
	static const uint8_t returnCode = 1;
	static const uint8_t mimimumLength = 0;

	static char kPortNumber[6];
	static const int kBufferLength = 512;

private:
	std::string m_argv;
	int m_initializeResult;

	SOCKET m_connectSocket;

	addrinfo *m_ptr;
	addrinfo *m_result;

	int receivedBufferLength;
	char receivedBuffer[kBufferLength] = "";
};