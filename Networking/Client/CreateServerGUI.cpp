#include "CreateServerGUI.h"

CreateServerGUI::CreateServerGUI()
{
	m_backgroundTexture = new ImageObject("assets/server_created.jpg");
	logger.Log("Menu backround texture created", Logger::Level::Info);
}

ApplicationStatus CreateServerGUI::Run()
{
	if (IsRunning())
	{
		SDL_RenderClear(RendererManager::m_renderer);

		m_backgroundTexture->Render();

		SDL_RenderPresent(RendererManager::m_renderer);

		return::ApplicationStatus::InCreateServer;
	}

	return ApplicationStatus::Exit;
}
