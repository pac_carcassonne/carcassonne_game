#include "Client.h"

char Client::kPortNumber[6] = "27015";

Client::Client()
{
	receivedBufferLength = kBufferLength;
}

Client::Client(const std::string& argv, addrinfo* ptr, addrinfo* result) :
	m_argv(argv),
	m_ptr(ptr),
	m_result(result)
{
	receivedBufferLength = kBufferLength;
}

Client::~Client()
{
	if (m_ptr != nullptr)
	{
		delete m_ptr;
	}

	if (m_result != nullptr)
	{
		delete m_result;
	}
}

Client::Client(const Client& other)
{
	this->m_argv = other.m_argv;
	this->m_connectSocket = other.m_connectSocket;
	this->m_initializeResult = other.m_initializeResult;
	this->m_ptr = other.m_ptr;
	this->m_result = other.m_result;

	receivedBufferLength = kBufferLength;
}

void Client::SetIP(const std::string& ip)
{
	m_argv =  ip;
}

// *** initialize Winsock ***
void Client::InitializeWinsocket()
{
	try
	{
		WSADATA wsaData;

		// initiate use of WS2_32.dll, with version 2.2
		m_initializeResult = WSAStartup(MAKEWORD(versionNumber, versionNumber), &wsaData);

		if (m_initializeResult != mimimumLength)
		{
			throw "WSAStartup failed! iResult != 0";
		}
	}
	catch (const char* errorMessage)
	{
		std::cout << errorMessage << std::endl;
	}
}

// *** Create a socket ***
void Client::CreateSocket()
{
	try
	{
		addrinfo hints;

		ZeroMemory(&hints, sizeof(hints));	// memset to 0
		hints.ai_family = AF_UNSPEC;		// unspecified: either an IPv6 or IPv4
		hints.ai_socktype = SOCK_STREAM;	// stream socket
		hints.ai_protocol = IPPROTO_TCP;	// TCP protocol

		// Resolve the server address and port
		m_initializeResult = getaddrinfo(m_argv.c_str(), kPortNumber, &hints, &m_result);

		if (m_initializeResult != mimimumLength)
		{
			WSACleanup();	// note: use WSACleanup when done working with sockets

			throw "getaddrinfo() failed: m_initializeResult != 0";
		}

		m_connectSocket = INVALID_SOCKET;
		// Attempt to connect to the first address returned by the call to getaddrinfo
		m_ptr = m_result;

		// Create a SOCKET for connecting to server
		m_connectSocket = socket(m_ptr->ai_family, m_ptr->ai_socktype, m_ptr->ai_protocol);

		if (m_connectSocket == INVALID_SOCKET)
		{
			std::cout << "Error at socket(): " << WSAGetLastError() << std::endl;

			freeaddrinfo(m_result);

			WSACleanup();
		}
	}
	catch (const char* errorMessage)
	{
		std::cout << errorMessage << std::endl;
	}
}

// *** Connect to server ***
void Client::ConnectToServer()
{
	try
	{
		m_initializeResult = connect(m_connectSocket, m_ptr->ai_addr, (int)m_ptr->ai_addrlen);

		if (m_initializeResult == SOCKET_ERROR)
		{
			closesocket(m_connectSocket);

			m_connectSocket = INVALID_SOCKET;
		}

		// Should really try the next address returned by getaddrinfo
		// if the connect call failed
		// But for this simple example we just free the resources
		// returned by getaddrinfo and print an error message

		freeaddrinfo(m_result);

		if (m_connectSocket == INVALID_SOCKET)
		{
			WSACleanup();

			throw "Unable to connect to server!";
		}
	}
	catch (const char* errorMessage)
	{
		std::cout << errorMessage << std::endl;
	}
}

// *** Send data to server ***
void Client::SendData()
{
	try
	{
		const char * sendBuffer = "This is a test!";
		int bufferSize = strlen(sendBuffer);

		// Send an initial buffer
		m_initializeResult = send(m_connectSocket, sendBuffer, bufferSize, mimimumLength);

		if (m_initializeResult == SOCKET_ERROR)
		{
			std::cout << "Send failed: " << WSAGetLastError() << std::endl;

			closesocket(m_connectSocket);

			WSACleanup();
		}

		std::cout << "Bytes Sent: " << m_initializeResult << std::endl;

	}
	catch (const std::exception& currentException)
	{
		currentException.what();
	}
}

void Client::ShutdownSending()
{
	try
	{
		// shutdown the connection for sending since no more data will be sent
		// the client can still use the ConnectSocket for receiving data
		m_initializeResult = shutdown(m_connectSocket, SD_SEND);

		if (m_initializeResult == SOCKET_ERROR)
		{
			std::cout << "Shutdown failed: " << WSAGetLastError() << std::endl;

			closesocket(m_connectSocket);

			WSACleanup();
		}
	}
	catch (const char* errorMessage)
	{
		std::cout << errorMessage << std::endl;
	}
}

void Client::ReceiveData()
{
	try
	{ 
		// Receive data until the server closes the connection
		do
		{
			m_initializeResult = recv(m_connectSocket, receivedBuffer, receivedBufferLength, mimimumLength);

			if (m_initializeResult > mimimumLength)
			{
				std::cout << "Bytes received: " << m_initializeResult << std::endl;

				std::cout << "Buffer content: " << receivedBuffer << std::endl;
			}
			else
			{
				if (m_initializeResult == mimimumLength)
				{
					std::cout << "Connection closed." << std::endl;
				}
				else
				{
					std::cout << "Received failed: " << WSAGetLastError() << std::endl;
				}
			}

		} while (m_initializeResult > mimimumLength);

	}
	catch (const char* errorMessage)
	{
		std::cout << errorMessage << std::endl;
	}
}

// *** cleanup ***
void Client::Cleanup()
{
	closesocket(m_connectSocket);

	WSACleanup();
}
