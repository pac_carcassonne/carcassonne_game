#pragma once

#include "WindowContent.h"

class OnlineMenuGUI : public WindowContent
{
public:
	//Initialize online menu GUI
	OnlineMenuGUI();

	//run online menu GUI (infinite loop)
	ApplicationStatus Run() override;

private:
	ImageObject* m_backgroundTexture;
	ImageObject* m_createServerButton;
	ImageObject* m_joinServerButton;
};