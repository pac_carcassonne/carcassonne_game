#include"Client.h"

void ValidateParameters(int argc, char* argv[])
{
	try
	{
		if (argc != Client::numberOfParameters)
		{
			std::cout << "Usage: " << argv[0] << " server-name" << std::endl;
		}
	}
	catch (const std::exception& currentException)
	{
		currentException.what();
	}
}

int main(int argc, char* argv[]) 
{
	ValidateParameters(argc, argv);

	Client client ("169.254.47.33");

	// *** initialize Winsock ***
	client.InitializeWinsocket();

	// *** Create a socket ***
	client.CreateSocket();

	// *** Connect to server ***
	client.ConnectToServer();

	// *** Send data to server ***
	client.SendData();

	client.ShutdownSending();

	client.ReceiveData();

	// *** cleanup ***
	client.Cleanup();

	return 0;
}